﻿using API_RESTful_01.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_RESTful_01.Contexts
{
    public interface IAppDbContext
    {
        DbSet<v_anhio> v_anhio();

    }
}
