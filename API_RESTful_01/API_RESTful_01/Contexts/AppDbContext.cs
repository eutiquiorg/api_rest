﻿using API_RESTful_01.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_RESTful_01.Contexts
{
    public class AppDbContext: DbContext//, IAppDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<v_anhio> v_anhio { get; set; }
        public DbSet<v_municipio> v_municipio { get; set; }
        public DbSet<v_tipo> v_tipo { get; set; }
        public DbSet<v_subtipo> v_subtipo { get; set; }
        public DbSet<v_modalidad> v_modalidad { get; set; }

        /*DbSet<v_anhio> IAppDbContext.v_anhio()
        {
            throw new NotImplementedException();
        }*/
    }
}
