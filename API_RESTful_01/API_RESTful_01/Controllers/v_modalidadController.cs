﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_RESTful_01.Contexts;
using API_RESTful_01.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_RESTful_01.Controllers
{
    [Route("api/[controller]")]
    public class v_modalidadController : Controller
    {private readonly AppDbContext context;

        public v_modalidadController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<v_modalidad> Get()
        {
            return context.v_modalidad.ToList();
        }


        // GET api/<controller>/5
        [HttpGet("{id}")]
        public v_modalidad Get(string id)
        {
            var producto = context.v_modalidad.FirstOrDefault(p => p.modalidad == id);
            return producto;
        }
    }
}
