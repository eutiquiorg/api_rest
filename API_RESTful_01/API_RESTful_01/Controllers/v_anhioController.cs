﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_RESTful_01.Contexts;
using API_RESTful_01.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_RESTful_01.Controllers
{
    [Route("api/[controller]")]
    public class v_anhioController : Controller
    {

        private readonly AppDbContext context;

        public v_anhioController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<v_anhio> Get()
        {
            return context.v_anhio.ToList();
        }


        // GET api/<controller>/5
        [HttpGet("{id}")]
        public v_anhio Get(int id)
        {
            var producto = context.v_anhio.FirstOrDefault(p => p.anhio == id);
            return producto;
        }
    }
}
