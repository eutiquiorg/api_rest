﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_RESTful_01.Entities
{
    public class v_municipio
    {
        [Key]
        public string  municipio { get; set; }
    }
}
